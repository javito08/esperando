class CalendarController < ApplicationController
  before_action :set_date

  def index
    @bets = Bet.all
  end

  private

  def set_date
    params[:start_date] ||= ENV['START_DATE'] || Date.today
  end
end
