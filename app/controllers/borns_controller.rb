class BornsController < ApplicationController
  before_action :set_born, only: [:show]
  before_action :set_date, only: [:show]

  def index
    @borns = Born.all
  end

  def new
    @born = Born.new
  end

  def create
    @born = Born.new(permited_params)

    if @born.save
      redirect_to "/#{@born.name}", notice: "Esperando a #{@born.name}, a compartir!!!"
    else
      render :new
    end
  end

  def show
  end

  private

  def set_born
    @born ||= params[:name].present? ? Born.find_by(name: params[:name]) : Born.find(params[:id])
  end

  def set_date
    params[:start_date] ||= @born.birthday || Date.today
  end

  def permited_params
    params.require(:born).permit(:name, :birthday, :description)
  end
end
