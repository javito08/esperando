class CreateBorns < ActiveRecord::Migration[5.0]
  def change
    create_table :borns do |t|
      t.date :birthday
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
