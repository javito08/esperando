class AddBornIdToBets < ActiveRecord::Migration[5.0]
  def change
    add_reference :bets, :born, foreign_key: true
  end
end
